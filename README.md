# Bristol Council spending visualisation

Code to visualise council spending over £500, available from https://opendata.bristol.gov.uk/explore/dataset/spend-over-ps500

**You can view the results of this code at https://mark-grimes.gitlab.io/bristolcitycouncilspending/.**

# Licence

All code in this repository is released under the [MIT licence](https://opensource.org/licenses/MIT).
The spending data (which is not in this repository) is under a separate licence; last time I checked
[Open Government Licence v3.0](http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/).

# Workflow

The raw data in JSON form is about 300k records, or 100Mb. It is pre-processed with the `spendDataCleanse.py`
python script to create a ~300kb file listing the total paid to each company/person, resulting in the
`spend-total.json` file. This is served to a page running a d3.js script to visualise it.

To test the server aspect, you can run a web server serving the `web/` folder.

For example, if you have Docker installed:

```
# Download the raw data from the Bristol City Council website (about 100Mb)
curl -o spend-over-ps500.json https://opendata.bristol.gov.uk/explore/dataset/spend-over-ps500/download/?format=json&timezone=UTC

# Run the python script to condense the data (storing output in web/spend-total.json)
python spendDataCleanse.py

# Run a webserver using docker to serve all the content in the web folder
docker run --name nginx -v $PWD/web:/usr/share/nginx/html:ro -d -p 8080:80 markgrimes/nginx-nocache
```

... and then point your browser at http://localhost:8080.
