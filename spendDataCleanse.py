# coding=utf-8

import json, datetime

with open('spend-over-ps500.json') as inputFile:
    input=json.load(inputFile)
print "JSON input loaded, there are", len(input), "records"

byCompany={}
byDescription={}

# Create some variables to check I've totalled everything correctly
allEntriesTotal=0;

for index in range(len(input)):
    entry=input[index]["fields"]
    allEntriesTotal+=entry["amount"]

    byCompany.setdefault( entry['name'], [] ).append( entry )
    byDescription.setdefault( entry['desc1'], [] ).append( entry )

totalPaid=[]
# Lump all really small payments into 1
miscSmallAmounts=0

for companyName, entries in byCompany.iteritems():
    total=0
    totalByYear={}
    for entry in entries:
        if entry["name"]!=companyName:
            raise Exception("An entry for '"+entry["name"]+"' was lumped in with '"+companyName+"'")
        total=total+entry["amount"]
        paydate=datetime.datetime.strptime( entry["paydate"], "%Y-%m-%d" )
        totalByYear[paydate.year]=totalByYear.get( paydate.year, 0 )+entry["amount"]

    if total<100000:
        miscSmallAmounts+=total
    else:
        totalPaid.append( {"name" :companyName, "total": total, "by_year": [] } )
        for year in sorted(totalByYear):
            totalPaid[-1]["by_year"].append( {"year": year, "total": totalByYear[year] } )

if miscSmallAmounts>0:
    totalPaid.append( {"name" :"Miscellaneous (individual totals less than £100k)", "total": miscSmallAmounts } )

# Make sure all the totals add up to the correct amount (N.B. round to integer to only check to nearest pound)
individualCompanyTotals=0
for companyEntry in totalPaid : individualCompanyTotals+=companyEntry["total"]
if int(individualCompanyTotals)!=int(allEntriesTotal): raise Exception("The total for all companies ("+str(individualCompanyTotals)+") does not match the total for all records ("+str(allEntriesTotal)+")!")

with open('web/spend-total.json','w') as outputFile:
    json.dump(totalPaid,outputFile)
